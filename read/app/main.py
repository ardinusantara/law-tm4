from fastapi import FastAPI
from app.database import meta, database, engine
from app.api import read_api

meta.create_all(engine)

app = FastAPI()

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

app.include_router(read_api, prefix='/read', tags=['read'])
