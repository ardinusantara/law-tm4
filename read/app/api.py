from fastapi import APIRouter, HTTPException
from app.models import Mahasiswa, ResponseMahasiswa
from app.crud import add_mahasiswa, get_mahasiswa

read_api = APIRouter()


@read_api.get("/")
async def hello_world():
    return {"message": "Ini Service Read"}


@read_api.post("/create", response_model=Mahasiswa, status_code=201)
async def create_mahasiswa(data: Mahasiswa):
    await add_mahasiswa(data)
    return data.dict()


@read_api.get("/{npm}", response_model=ResponseMahasiswa, status_code=200)
async def retrieve_mahasiswa(npm: str):
    mhs = await get_mahasiswa(npm)
    if not mhs:
        raise HTTPException(status_code=404, detail=f"Mahasiswa dengan npm {npm} tidak ditemukan")
    return {
        "status":"OK",
        **mhs
    }
