from app.models import Mahasiswa
from app.database import mahasiswa, database
from sqlalchemy import insert, select

async def add_mahasiswa(data: Mahasiswa):
    query = mahasiswa.insert().values(**data.dict())
    return await database.execute(query=query)

async def get_all_mahasiswa():
    query = select(Mahasiswa).all()
    return await database.execute(query=query)

async def get_mahasiswa(npm: str):
    query = mahasiswa.select(mahasiswa.c.npm == npm)
    return await database.fetch_one(query=query)