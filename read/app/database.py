import os

from sqlalchemy import Column, String, MetaData, Table, create_engine
from databases import Database

DATABASE_URL = os.getenv("DATABASE_URL")

engine = create_engine(DATABASE_URL)
meta = MetaData()

mahasiswa = Table(
    'mahasiswa', meta, Column('npm', String, primary_key=True), Column('nama', String)
)

database = Database(DATABASE_URL)
