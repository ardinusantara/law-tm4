from pydantic import BaseModel


class Mahasiswa(BaseModel):
    npm: str
    nama: str
    
class ResponseMahasiswa(Mahasiswa):
    status: str