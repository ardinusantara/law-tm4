import os

from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import create_engine

DATABASE_URL = os.getenv("DATABASE_URL")

engine = create_engine(DATABASE_URL)

class Mahasiswa(BaseModel):
    npm: str
    nama: str

async def update_mahasiswa(mahasiswa: Mahasiswa):
    with engine.connect() as con:
        con.execute(f"UPDATE mahasiswa SET nama='{mahasiswa.nama}' WHERE npm='{mahasiswa.npm}'")

app = FastAPI()

@app.post("/update", status_code=200)
async def update_mhs(mahasiswa: Mahasiswa):
    await update_mahasiswa(mahasiswa)
    return {"status":"OK"}
